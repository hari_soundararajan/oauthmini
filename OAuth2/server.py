from flask import Flask
app = Flask(__name__)


@app.route('/geturl/<client_id>/secret/<client_secret>')
def auth_url(client_id, client_secret):
    if client_id == "684080750610" and client_secret == 'IYoVzsW7O':
        url = 'http://127.0.0.1:5000/authcode/auth/1324frece'
        return url
    else:
        return "Unauthorized"


@app.route('/authcode/234bfwnwc/user/<username>/pass/<password>')
def auth_code(username, password):
    if username == 'hari' and password == 'pass':
        return "Your Authentication token is: 6MVxZkyjfL_EkB5Fg  refresh token is: xZky3r22EkB5Fg"
    else:
        return "Unauthorized"


@app.route('/authcode/auth/<authcode>')
def get_auth_code(authcode):
    if authcode == '1324frece':
        return "The url for building Oauth is : http://127.0.0.1:5000/authcode/234bfwnwc/user/(username)/pass/(password)" \
               "  -> Include username and password "
    else:
        return "Unauthorized"


@app.route('/token/<authcode>')
def get_toke (authcode):
    if authcode == '6MVxZkyjfL_EkB5Fg':
        return "wfnwfwfwssae"
    else:
        return "Unauthorized"


@app.route('/getemail/token/<authcode>')
def get_profile_email(authcode):
    if authcode == '6MVxZkyjfL_EkB5Fg':
        return "The email is noreply@quanticmind"
    else:
        return "Unauthorized"

if __name__ == '__main__':
    app.run()
