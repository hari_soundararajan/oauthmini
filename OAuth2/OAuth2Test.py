import requests
import urllib

scope = u'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'

client_id = "684080750610-m5n6br5odim1ae1h97j4gnppd0l7r3s5.apps.googleusercontent.com"
client_secret = "-oxIYoVzsW7OHshnvgJ8mO5F"
base_url = "https://accounts.google.com/o/oauth2/auth"
redirect_uri = u"urn:ietf:wg:oauth:2.0:oob"
response_type = "code"

url = "{base_url}?access_type=offline&client_id={client_id}&redirect_uri={redirect_uri}&response_type={response_type}&scope={scope}".format(
    base_url=base_url,
    client_id=client_id,
    redirect_uri=urllib.quote_plus(redirect_uri.encode('utf8')),
    response_type=response_type,
    scope=urllib.quote_plus(scope.encode('utf8')))

print "Paste the following url in the browser"
print url

authorization_code = raw_input("Enter the authorization code: ")

url_for_profile = "https://www.googleapis.com/oauth2/v3/userinfo"

access_token_uri = 'https://accounts.google.com/o/oauth2/token'

params = {
    'code':authorization_code,
    'redirect_uri':redirect_uri,
    'client_id':client_id,
    'client_secret':client_secret,
    'grant_type': 'authorization_code'
}
headers={'content-type': 'application/x-www-form-urlencoded'}
r = requests.post(access_token_uri, data=params)

data = r.json()
access_token = data['access_token']

authorization_header = {"Authorization": "OAuth %s" % access_token}
r = requests.get("https://www.googleapis.com/oauth2/v2/userinfo",
                 headers=authorization_header)

print "Your basic info is: "
print r.text